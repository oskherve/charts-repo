import { ComponentFixture, TestBed } from '@angular/core/testing';

import { C3jsComponent } from './c3js.component';

describe('C3jsComponent', () => {
  let component: C3jsComponent;
  let fixture: ComponentFixture<C3jsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ C3jsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(C3jsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
