import { Component, Input, OnInit } from '@angular/core';
import { ChartType } from 'angular-google-charts';


export interface chartModel {
  title: string;
  type: ChartType;
  data: any;
  columns: string;
  options: any;
  width: any;
  height: any;
}

@Component({
  selector: 'app-google-charts',
  templateUrl: './google-charts.component.html',
  styleUrls: ['./google-charts.component.scss']
})
export class GoogleChartsComponent implements OnInit {

  @Input()
  chart = {} as chartModel;

  pieChart = ChartType.PieChart;
  donutOptions = {
    title: 'My Daily Activities',
    pieHole: 0.5,
  };
  gaugeChart = ChartType.Gauge;
  lineChart = ChartType.LineChart;
  barChart = ChartType.BarChart;

  data = [
    ['READY', 1100],
    ['DONE', 90],
    ['FAILED', 10]
  ]

  constructor() { }

  ngOnInit(): void {
    this.chart.title = 'Dossier par status';
    this.chart.type = ChartType.PieChart;
    this.chart.data = this.data;
    this.chart.columns = '';
    this.chart.options = this.donutOptions;
    this.chart.width = 150;
    this.chart.height = 150;
  }

}
