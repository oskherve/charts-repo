import { Component, Input, OnInit } from "@angular/core";
import { ChartType } from "angular-google-charts";
import {
  PIE_DATA_1,
  PIE_DATA_2,
  PIE_DATA_3,
  PIE_DATA_4,
  PIE_DATA_5,
  PIE_DATA_6,
} from "../data";
import { chartModel } from "../google-charts/google-charts.component";

@Component({
  selector: "app-ngx-charts",
  templateUrl: "./ngx-charts.component.html",
  styleUrls: ["./ngx-charts.component.scss"],
})
export class NgxChartsComponent implements OnInit {
  /**
   * * * * ngx-chart * * * *
   */
  data1 = PIE_DATA_1;
  data2 = PIE_DATA_2;
  data3 = PIE_DATA_3;
  data4 = PIE_DATA_4;
  data5 = PIE_DATA_5;
  data6 = PIE_DATA_6;

  chartCanvas: any = [180, 180];

  // options
  gradient: boolean = false;
  showLegend: boolean = false;
  showLabels: boolean = false;
  isDoughnut: boolean = true;
  chartWidth = 0.1;
  legendPosition: any = "below";

  colorScheme1: any = {
    domain: ["#fafbab", "#66717e"],
  };
  colorScheme2: any = {
    domain: ["#95f283", "#66717e"],
  };
  colorScheme3: any = {
    domain: ["#7ad1e4", "#66717e"],
  };
  colorScheme4: any = {
    domain: ["#f7f758", "#66717e"],
  };
  colorScheme5: any = {
    domain: ["#ed86d0", "#66717e"],
  };
  colorScheme6: any = {
    domain: ["#ff9900", "#66717e"],
  };

  /**
   * * * * Google chart * * * *
   */
  @Input()
  gChart = {} as chartModel;
  gaugeChart = ChartType.Gauge;
  lineChart = ChartType.LineChart;

  /// google pie chart
  pieChart = ChartType.PieChart;
  donutOptions = {
    // title: 'My Daily Activities',
    pieHole: 0.5,
    chartArea: {
      top: 20,
      left: 10,
      height: "90%",
      width: "90%",
    },
    legend: {
      position: "right",
      textStyle: { color: "#fff" },
    },
    backgroundColor: { fill: "transparent" },
    tooltip: { trigger: "none" },
    pieSliceBorderColor: "none",
  };

  gData = [
    ["My all", 100],
    ["READY", 1100],
    ["DONE", 90],
    ["test 1", 30],
    ["test 2", 100],
    ["test 3", 200],
  ];

  //// google bar chart
  barChart = ChartType.BarChart;
  barChartData: any = [
    ["OK", "Warning", "KO", { role: "annotation" }],
    ["GP", 10, 24, 20, ""],
    ["PP", 16, 22, 23, ""],
  ];

  barChartOptions = {
    width: 600,
    height: 400,
    legend: { position: "top", maxLines: 3 },
    bar: { groupWidth: "75%" },
    isStacked: true,

    // chartArea: {
    //   top: 20,
    //   left: 10,
    //   height: '90%',
    //   width: '90%',
    // },
    // legend: {
    //   position: 'right',
    //   textStyle: { color: '#fff' },
    // },
    // backgroundColor: { fill: 'transparent' },
    // tooltip: { trigger: 'none' },
    // pieSliceBorderColor: 'none'
  };

  // barChartTitle = "GP/PP";
  type = "BarChart";
  barChartData2 = [
    ["GP", 32, 10, 58],
    ["PP", 40, 5, 55],
  ];
  columnNames = ["OK", "Warning", "KO"];
  barChartOptions2 = {
    hAxis: {
      // title: "Year",
      textStyle: { color: "#fff" }
    },
    vAxis: {
      minValue: 0,
      textStyle: { color: "#fff" }
    },
    isStacked: true,
    // chartArea: {
    //   top: 20,
    //   left: 10,
    //   height: "90%",
    //   width: "90%",
    // },
    legend: {
      position: "right",
      textStyle: { color: "#fff" },
    },
    backgroundColor: { fill: "transparent" },
    tooltip: { trigger: "none" },
  };

  /// angular gauge
  percentageValue: (value: number) => string;

  gaugeValues: any = {
    1: 100,
    2: 50,
    3: 50,
    4: 50,
    5: 50,
    6: 50,
    7: 50,
  };

  interval: any;

  constructor() {
    this.percentageValue = function (value: number): string {
      return `${Math.round(value)} / 100`;
    };
  }

  // constructor() {}

  ngOnInit(): void {
    /**
     * * * * Google chart data * * * *
     */
    // this.gChart.title = 'Dossier par status';
    this.gChart.type = this.pieChart;
    this.gChart.data = this.gData;
    this.gChart.options = this.donutOptions;
    this.gChart.width = "100%";
    this.gChart.height = "100%";

    /// angular gauge
    const updateValues = (): void => {
      this.gaugeValues = {
        1: Math.round(Math.random() * 100),
        2: Math.round(Math.random() * 100),
        3: Math.round(Math.random() * 100),
        4: Math.round(Math.random() * 100),
        5: Math.round(Math.random() * 200),
        6: Math.round(Math.random() * 100),
        7: Math.round(Math.random() * 100),
      };
    };

    const INTERVAL: number = 5000;

    this.interval = setInterval(updateValues, INTERVAL);
    updateValues();
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  onSelect(data: any): void {
    console.log("Item clicked", JSON.parse(JSON.stringify(data)));
  }

  onActivate(data: any): void {
    console.log("Activate", JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data: any): void {
    console.log("Deactivate", JSON.parse(JSON.stringify(data)));
  }
}
