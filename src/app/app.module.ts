import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GoogleChartsComponent } from './google-charts/google-charts.component';
import { ChartjsComponent } from './chartjs/chartjs.component';
import { NgxChartsComponent } from './ngx-charts/ngx-charts.component';
import { EchartsComponent } from './echarts/echarts.component';
import { D3jsComponent } from './d3js/d3js.component';
import { C3jsComponent } from './c3js/c3js.component';
import { PieComponent } from './charts-types/pie/pie.component';
import { DonutComponent } from './charts-types/donut/donut.component';
import { GaugeComponent } from './charts-types/gauge/gauge.component';
import { BarComponent } from './charts-types/bar/bar.component';
import { LineComponent } from './charts-types/line/line.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GaugeModule } from 'angular-gauge';

@NgModule({
  declarations: [
    AppComponent,
    PieComponent,
    DonutComponent,
    GaugeComponent,
    BarComponent,
    LineComponent,
    GoogleChartsComponent,
    ChartjsComponent,
    NgxChartsComponent,
    EchartsComponent,
    D3jsComponent,
    C3jsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GoogleChartsModule,
    NgxChartsModule,
    GaugeModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
